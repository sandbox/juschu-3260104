<?php

namespace Drupal\body_zoom\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Form\ThemeSettingsForm;

/**
 * Build global settings form.
 */
class ThemeSettingsExtensions extends ThemeSettingsForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $theme = '') {

    // Get the original form.
    $form = parent::buildForm($form, $form_state);

    $config = \Drupal::configFactory()
      ->getEditable('system.theme.global');

    // Add form elements.
    $form['details'] = [
      '#type' => 'details',
      '#title' => $this->t('User Interface'),
    ];

    $form['details']['zoom_level'] = [
      '#type' => 'number',
      '#title' => t('Global Zoom Level'),
      '#description' => t('Determine the global zoom level in percent (For example: 80 = 80%).'),
      '#maxlength' => 3,
      '#max' => 150,
      '#min' => 50,
      '#default_value' => $config->get('zoom_level') ?: 100,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Save zoom value as global value in config.
    $config = \Drupal::configFactory()
      ->getEditable('system.theme.global');

    $config->set('zoom_level', $form_state->getValue('zoom_level'));

    $config->save();
    parent::submitForm($form, $form_state);
  }

}
