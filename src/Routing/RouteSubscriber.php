<?php

namespace Drupal\body_zoom\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {

    // Overwrite existing path with custom one.
    if ($route = $collection->get('system.theme_settings')) {
      $route->setDefault('_form', '\Drupal\body_zoom\Form\ThemeSettingsExtensions');
    }
  }

}
