<?php

namespace Drupal\body_zoom;

use Twig\Extension\ExtensionInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Extends twig functions.
 */
class BodyZoomTwigFunctions extends AbstractExtension implements ExtensionInterface {

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return 'body_zoom.twig_functions';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array {
    return [
      new TwigFunction('body_zoom_critical_css', [$this, 'bodyZoom']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function bodyZoom(): string {

    // Get zoom value from user data and write value as critical css.
    $user_data = \Drupal::service('user.data');
    $global_config = \Drupal::config('system.theme.global');
    $global_config = $global_config->get('zoom_level');
    $user_config = $user_data->get('body_zoom', \Drupal::currentUser()
      ->id(), 'zoom_level');

    // If no local value is set, global value is standard.
    if (!empty($user_config)) {
      $global_config = $user_config;
    }
    return "<style>html { zoom: {$global_config}% }</style>";
  }

}
