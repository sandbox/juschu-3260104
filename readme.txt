INTRODUCTION
------------

The Body Zoom module is a custom addition on top of Drupal's
theme "Gin Theme Admin", allowing you to adjust the size of
the theme interface as a default globally and of your own user interface
with minimal effort.

This module is intended as an addition for the gin theme and does not
provide meaningful functionality on its own for site-builders.


REQUIREMENTS
------------

This module requires the installation of the "Gin Admin Theme".
https://www.drupal.org/project/gin


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/documentation/install/modules-themes/modules-8
for further information.

* Add "{{ body_zoom_critical_css()|raw }}" into the <head> tag of your themes html.html.twig file


CONFIGURATION
-------------

* Configure the module as a global default in Appearance » Settings:

    "User Interface": Toggle this tab to show the "Global Zoom Level" field.
    The number you type in stands for the value of the html body in percent.
    You can type a number between 50 and 150.


* Configure the module with a personal value in User » Edit:

    "User Interface": Toggle this tab to show "Zoom Level" field.
    The number you type in stands for the value of the html body in percent.
    You can type a number between 50 and 150.


USAGE
-----

The module is intended for users of the gin theme as an addition for their
personalisation.


